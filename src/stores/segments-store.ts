/* eslint-disable no-underscore-dangle */
import { objstore } from '@vedavaapi/client';

import { BufferredGraphStore } from './bufferred-graph-store';
import { getSegmentsGraph } from '../vedavaapi-textract';
import { structuredClone } from './graph-helper';

export class SegmentsStore extends BufferredGraphStore {
    private async loadSegments() {
        if (!this.loadPromise) {
            this.loadPromise = getSegmentsGraph(this.vvinfo.vc, [this.pageId]);
        }
        await this.resolveLoadPromise();
    }

    public async load(refresh = false) {
        if (!this.gResp) {
            await this.loadSegments();
            this.emit(SegmentsStore.RESOURCES_LOADED);
        } else if (refresh) {
            await this.loadSegments();
            this.emit(SegmentsStore.RESOURCES_REFRESHED);
        }
    }

    public async getSegmentsSubGraph(regionIds: string[]): Promise<objstore.rms.GraphResponse> {
        const sgResp = await this.getSubGraph(regionIds, 1);
        const page = await structuredClone(this.gResp.graph[this.pageId]);
        if (!page) {
            sgResp.start_nodes_ids = [];
            return sgResp;
        }
        sgResp.start_nodes_ids = [page._id as string];
        page._reached_ids.source = regionIds.filter((rid) => Object.prototype.hasOwnProperty.call(sgResp.graph, rid));
        sgResp.graph[page._id as string] = page;
        return sgResp;
    }
}
