/* eslint-disable dot-notation */
/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
import delve from 'dlv';
// @ts-ignore
import RTree from 'rtree';
import { OID } from '@vedavaapi/types/dist-esm/JsonObject';
import { IResource } from '@vedavaapi/types/dist-esm/Resource';
import { bbox } from '@vedavaapi/types/dist-esm/SvgFragmentSelectorChoice';
import { IBBox } from '@vedavaapi/types/dist-esm/FragmentSelector';
import { IIDResourceMap } from '@vedavaapi/client/dist/api/api-param-models';
import { objstore } from '@vedavaapi/client';
import { getLayoutGraph } from '../vedavaapi-textract';
import { structuredClone, mergeUpdatesToResources } from './graph-helper';
import { GraphStore } from './graph-store';

export function renameKey(obj: any, oldKey: string|symbol, newKey: string|symbol) {
    delete Object.assign(obj, { [newKey]: obj[oldKey] })[oldKey];
}

export interface IIIFAnno {
    '@id': string;
    '@type': string;
    [x: string]: any;
}

export interface IIDIIIFAnnoMap {
    [id: string]: IIIFAnno;
}

export const HOCR_CLASS_TO_STATEMENT_PREFIX_MAP = {
    HOCRPage: 'page',
    HOCRCArea: 'b',
    HOCRParagraph: 'p',
    HOCRLine: 'l',
};

export const STATEMENT_PREFIX_TO_HOCR_CLASS_MAP = {
    page: 'HOCRPage',
    b: 'HOCRCArea',
    p: 'HOCRParagraph',
    l: 'HOCRLine',
};

export const HOCR_CLASS_TO_COLOR_MAP = {
    HOCRPage: '#ff0000',
    HOCRCArea: '#00bfff',
    HOCRParagraph: '#008000',
    HOCRLine: '#1f36e6',
};

export function hocrElemToStatementText(hocrElem: {jsonClassLabel?: string, index?: number}): string {
    // @ts-ignore
    let statementText = `${HOCR_CLASS_TO_STATEMENT_PREFIX_MAP[hocrElem.jsonClassLabel] || ''}${hocrElem.index !== undefined ? `-${hocrElem.index}` : ''}`;
    statementText = `<b>${statementText}</b>`;
    return statementText;
}

export function htmlToText(html: string): string {
    // https://stackoverflow.com/a/47140708/5925119
    const doc = new DOMParser().parseFromString(html, 'text/html');
    return doc.body.textContent || '';
}

export function getSvgWithStrokeColor(svg: string, color: string) {
    return svg.replace(/stroke="#[0-9a-fA-F]*"/, `stroke="${color}"`);
}

export function setStatementColor(statement: IIIFAnno) {
    // eslint-disable-next-line no-param-reassign
    statement.on[0].selector.item.value = getSvgWithStrokeColor(
        statement.on[0].selector.item.value,
        HOCR_CLASS_TO_COLOR_MAP[statement.jsonClassLabel as keyof typeof HOCR_CLASS_TO_STATEMENT_PREFIX_MAP],
    );
}


export function layoutResourceToStatement(res: IResource) {
    const statement = {
        '@id': res._id as string,
        jsonClass: 'HOCRElement', // NOTE
        jsonClassLabel: res.jsonClassLabel,
        '@type': 'oa:Annotation',
        motivation: ['oa:commenting'],
        resource: [{
            chars: hocrElemToStatementText(res),
            '@type': 'dctypes:Text',
            format: 'text/html',
        }],
        on: [{
            '@type': 'oa:SpecificResource',
            full: res.source,
            selector: JSON.parse(JSON.stringify(res.selector)),
        }],
        index: res.index,
    };
    [statement.on[0].selector, statement.on[0].selector.default, statement.on[0].selector.item].forEach((v) => {
        renameKey(v, 'type', '@type');
    });
    setStatementColor(statement);
    return statement;
}

export const STATEMENT_TEXT_REGEX = /^\s*([a-z]+) *[-_]* *([0-9]*)/;

export function iiifAnnoToStatement(anno: IIIFAnno) {
    // eslint-disable-next-line no-param-reassign
    anno.jsonClass = 'HOCRElement';
    const annoHTML = delve(anno, 'resource.0.chars');
    if (!annoHTML) throw Error('Layout statement should not be empty');
    const annoText = htmlToText(annoHTML).trim().toLowerCase();

    const matchResult = annoText.match(STATEMENT_TEXT_REGEX);
    if (!matchResult) {
        throw Error('Invalid layout statement');
    }
    const [, prefix, indexString] = matchResult;

    // @ts-ignore
    const hocrClass = STATEMENT_PREFIX_TO_HOCR_CLASS_MAP[prefix];
    if (!hocrClass) {
        throw Error('Invalid layout statement. Unknown element class');
    }
    const index = indexString ? Number(indexString) : undefined;

    // eslint-disable-next-line no-param-reassign
    anno.jsonClassLabel = hocrClass;
    // eslint-disable-next-line no-param-reassign
    anno.index = index;
    // @ts-ignore
    // eslint-disable-next-line no-param-reassign
    anno.resource[0].chars = hocrElemToStatementText(anno);
    anno.resource[0].jsonClass = 'Text';

    anno.on[0].selector.jsonClass = 'SvgFragmentSelectorChoice';
    anno.on[0].selector.default.jsonClass = 'FragmentSelector';
    anno.on[0].selector.item.jsonClass = 'SvgSelector';
    setStatementColor(anno);
    return anno;
}

export function statementToResource(statement: IIIFAnno) {
    const { selector } = statement.on[0];
    [selector, selector.default, selector.item].forEach((doc) => {
        // eslint-disable-next-line no-param-reassign
        delete doc['@type'];
    });
    const res: IResource = {
        jsonClass: 'Resource',
        jsonClassLabel: statement.jsonClassLabel,
        index: statement.index,
        selector,
        source: statement.on[0].full,
        _id: statement['@id'],
    };
    return res;
}

export function statementGraphToResourceGraph(statementsGraph: IIDIIIFAnnoMap) {
    const resGraph: IIDResourceMap = {};
    Object.values(statementsGraph).forEach((statement) => {
        const res = statementToResource(statement);
        resGraph[res._id as OID] = res;
    });
    return resGraph;
}

export function classifyHOCRElements(graph: IIDResourceMap): { [jcl: string]: IResource[] } {
    const result: { [jcl: string]: IResource[] } = {};
    Object.values(graph).forEach((res) => {
        if (res.jsonClass !== 'Resource') return;
        const jcl = res.jsonClassLabel;
        if (!jcl) return;
        if (!Object.prototype.hasOwnProperty.call(result, jcl)) {
            result[jcl] = [];
        }
        result[jcl].push(res);
    });
    return result;
}

export const HOCR_CLASS_LEVELS = ['HOCRPage', 'HOCRCArea', 'HOCRParagraph', 'HOCRLine'];

export const HOCR_CLASS_LEVELS_REVERSE = ['HOCRLine', 'HOCRParagraph', 'HOCRCArea', 'HOCRPage'];

export function containsBBox(bbox1: IBBox, bbox2: IBBox) {
    return (
        bbox1.x < bbox2.x && bbox1.y < bbox2.y
        && (bbox1.x + bbox1.w > bbox2.x + bbox2.w)
        && (bbox1.y + bbox1.h > bbox2.y + bbox2.h)
    );
}

/**
 * returns equal if there is minimum 60% vertical overlap
 * @param bbox1
 * @param bbox2
 */
export function compareWordBBoxesVertically(bbox1: IBBox, bbox2: IBBox) {
    if (bbox1.y === bbox2.y) return 0; // ideal case
    const [topBBox, bottomBBox] = bbox1.y < bbox2.y ? [bbox1, bbox2] : [bbox2, bbox1];
    const overlap = topBBox.y + topBBox.h - bottomBBox.y;
    if ((overlap / bbox1.h >= 0.6) && (overlap / bbox2.h >= 0.6)) return 0;
    return (topBBox === bbox1) ? -1 : 1;
}

export function compareWordBBoxes(bbox1: IBBox, bbox2: IBBox) {
    const verticalRelation = compareWordBBoxesVertically(bbox1, bbox2);
    if (verticalRelation !== 0) return verticalRelation;
    if (bbox1.x === bbox2.x) return 0;
    return (bbox1.x < bbox2.x) ? -1 : 1;
}

export async function computeHOCRHierarchyForNewResources(gResp: objstore.rms.GraphResponse, newGraph: IIDResourceMap, segmentsGResp: objstore.rms.GraphResponse) {
    const resolvedClassifications = classifyHOCRElements(gResp.graph);
    const unResolvedClassifications = classifyHOCRElements(newGraph);

    HOCR_CLASS_LEVELS.forEach((jcl) => {
        const newElems = unResolvedClassifications[jcl];
        if (!newElems || !newElems.length) return;
        const highLevels = HOCR_CLASS_LEVELS_REVERSE.slice(HOCR_CLASS_LEVELS_REVERSE.indexOf(jcl) + 1);
        newElems.forEach((elem) => {
            let parent : IResource | undefined;
            const elemBBox = bbox(elem.selector);
            highLevels.some((hl) => {
                const hlElems = resolvedClassifications[hl];
                if (!hlElems || !hlElems.length) return false;
                return hlElems.some((hlElem) => {
                    const hlElemBBox = bbox(hlElem.selector);
                    if (containsBBox(hlElemBBox, elemBBox)) {
                        parent = hlElem;
                        return true;
                    }
                    return false;
                });
            });
            if (parent !== undefined) {
                // @ts-ignore
                // eslint-disable-next-line no-param-reassign
                elem.source = parent._id;
                if (!elem.index) {
                    // eslint-disable-next-line no-param-reassign
                    elem.index = delve(parent, '_reached_ids.source', []).length;
                }
                // @ts-ignore
                parent._reached_ids = parent._reached_ids || {};
                // @ts-ignore
                parent._reached_ids.source = parent._reached_ids.source || [];
                // @ts-ignore
                parent._reached_ids.source.push(elem._id);
            }
            resolvedClassifications[jcl] = resolvedClassifications[jcl] || [];
            resolvedClassifications[jcl].push(elem);
        });
    });

    const pageId = segmentsGResp.start_nodes_ids[0];
    const regions = segmentsGResp.getReachedResources(pageId, 'source');
    const segmentsRtree = RTree();
    regions.forEach((region) => {
        const regionBBox = bbox(region.selector);
        segmentsRtree.insert(bbox(region.selector), { id: region._id, bbox: regionBBox });
    });
    // @ts-ignore
    window['srtree'] = segmentsRtree;

    Object.values(newGraph).forEach((elem) => {
        if (elem._reached_ids) {
            delete elem._reached_ids;
            return;
        }
        const elemBBox = bbox(elem.selector);
        const memberObjects: any[] = segmentsRtree.bbox(elemBBox.x, elemBBox.y, elemBBox.x + elemBBox.w, elemBBox.y + elemBBox.h);
        memberObjects.sort((o1, o2) => compareWordBBoxes(o1.bbox, o2.bbox));
        elem.members = { resource_ids: memberObjects.map((o) => o.id) };
    });

    return newGraph;
}

/**
 * This GraphStore layers over base GraphStore, and provides buffer for layout changes.
 * it buffers statements in form of mirador annos, not actual resources
 * when asked to flush, it compute layout resources from all buffered statements, and then saves them.
 */
export class LayoutStatementsStore extends GraphStore {
    private deletesBuffer: OID[];

    private updatesBuffer: IIDIIIFAnnoMap;

    private additionsBuffer: IIDIIIFAnnoMap;

    public static readonly BUFFER_DIRTY_STATUS_CHANGED = 'buffer-dirty-status-changed';

    public static readonly BUFFER_FLUSHED = 'buffer-flushed';

    constructor(vvinfo: any, pageId: string) {
        super(vvinfo, pageId);
        this.deletesBuffer = [];
        this.additionsBuffer = {};
        this.updatesBuffer = {};
    }

    private async loadLayout() {
        if (!this.loadPromise) {
            this.loadPromise = getLayoutGraph(this.vvinfo.vc, [this.pageId]);
        }
        await this.resolveLoadPromise();
    }

    public async load(refresh = false) {
        if (!this.gResp) {
            await this.loadLayout();
            this.emit(LayoutStatementsStore.RESOURCES_LOADED);
        } else if (refresh) {
            await this.loadLayout();
            this.emit(LayoutStatementsStore.RESOURCES_REFRESHED);
        }
    }

    public isBufferDirty(): boolean {
        return !!(this.deletesBuffer.length || Object.keys(this.updatesBuffer).length || Object.keys(this.additionsBuffer).length);
    }

    public async clearBuffer() {
        this.deletesBuffer = [];
        this.additionsBuffer = {};
        this.updatesBuffer = {};
        this.emit(LayoutStatementsStore.BUFFER_DIRTY_STATUS_CHANGED, false);
    }

    public async deleteLayoutElements() {
        await this.load();
        const hocrIds = Object.keys(this.gResp.graph);
        const pageIdIndex = hocrIds.indexOf(this.pageId);
        if (pageIdIndex !== -1) {
            hocrIds.splice(pageIdIndex, 1);
        }
        const deleteRespData = await this.deleteSubgraph(hocrIds);
        await this.clearBuffer();
        return deleteRespData;
    }

    public async wipe() {
        const deleteRespData = await this.deleteLayoutElements();
        await this.load(true);
        return deleteRespData;
    }

    public async statements(elementClasses: string[], mergeBuffer = false): Promise<IIDIIIFAnnoMap> {
        await this.load();
        const { gResp } = this; // as it will be read-only usage, no problem
        const page = gResp.graph[this.pageId];
        if (!page) return {};

        let statements: IIDIIIFAnnoMap = {};
        const addToStatements = (resources: IResource[]) => {
            resources.forEach((res) => {
                if (elementClasses.includes(res.jsonClassLabel as OID)) {
                    statements[res._id as OID] = layoutResourceToStatement(res);
                }
                const reachedResources = gResp.getReachedResources(res, 'source');
                addToStatements(reachedResources);
            });
        };

        addToStatements(gResp.getReachedResources(page, 'source'));
        statements = await structuredClone(statements);

        if (mergeBuffer) {
            await this.mergeBuffer(statements, elementClasses);
        }
        return statements;
    }

    public async addStatementsToBuffer(annos: IIIFAnno[]): Promise<IIIFAnno[]> {
        // NOTE we are modifying callee input though
        const statements: IIIFAnno[] = [];
        let idStatementsMap: IIDIIIFAnnoMap = {};
        const timestamp = Date.now();
        annos.forEach((anno, i) => {
            const statement = iiifAnnoToStatement(anno); // NOTE inplace updates anno
            const blankId = `_:${timestamp}-${i}`;
            statement['@id'] = blankId;
            statements.push(statement);
            idStatementsMap[blankId] = statement;
        });
        idStatementsMap = await structuredClone(idStatementsMap);
        Object.assign(this.additionsBuffer, idStatementsMap);
        if (statements.length) this.emit(LayoutStatementsStore.BUFFER_DIRTY_STATUS_CHANGED, true);
        return statements;
    }

    public async updateStatementsInBuffer(statements: IIIFAnno[]): Promise<IIIFAnno[]> {
        // TODO, merge this fn with above?
        const newStatements: IIIFAnno[] = [];
        let updatesIdStatementsMap: IIDIIIFAnnoMap = {};
        let additionsIdStatementsMap: IIDIIIFAnnoMap = {};
        statements.forEach((statement) => {
            const statementId = statement['@id'];
            const isInGResp = statementId && Object.prototype.hasOwnProperty.call(this.gResp.graph, statementId);
            const isInadditionsBuffer = statementId && Object.prototype.hasOwnProperty.call(this.additionsBuffer, statementId);
            if (!statementId || !(isInGResp || isInadditionsBuffer)) {
                throw Error('Invalid update operation. Resources should already exist in gResp or buffer to update them');
            }
            // eslint-disable-next-line no-param-reassign
            const newStatement = iiifAnnoToStatement(statement); // it is inplace, and both will be same
            if (isInGResp) {
                const oldResource = this.gResp.graph[statementId];
                if (oldResource.jsonClassLabel !== newStatement.jsonClassLabel) {
                    throw Error('You cannot change type of existing elements');
                }
            }
            newStatements.push(newStatement);
            (isInGResp ? updatesIdStatementsMap : additionsIdStatementsMap)[statementId] = newStatement;
        });
        updatesIdStatementsMap = await structuredClone(updatesIdStatementsMap);
        additionsIdStatementsMap = await structuredClone(additionsIdStatementsMap);
        Object.assign(this.updatesBuffer, updatesIdStatementsMap);
        Object.assign(this.additionsBuffer, additionsIdStatementsMap);
        if (newStatements.length) this.emit(LayoutStatementsStore.BUFFER_DIRTY_STATUS_CHANGED, true);
        return newStatements;
    }

    public async deleteStatementsInBuffer(statementIds: OID[]): Promise<OID[]> {
        const deletedStatementIds: OID[] = [];
        statementIds.forEach((id) => {
            if (Object.prototype.hasOwnProperty.call(this.additionsBuffer, id)) {
                delete this.additionsBuffer[id];
                deletedStatementIds.push(id);
                return;
            }
            if (Object.prototype.hasOwnProperty.call(this.gResp.graph, id)) {
                if (!this.deletesBuffer.includes(id)) this.deletesBuffer.push(id);
                deletedStatementIds.push(id);
            }
            if (Object.prototype.hasOwnProperty.call(this.updatesBuffer, id)) {
                delete this.updatesBuffer[id];
            }
        });
        if (deletedStatementIds.length) this.emit(LayoutStatementsStore.BUFFER_DIRTY_STATUS_CHANGED, true);
        return deletedStatementIds;
    }

    public async mergeBuffer(statements: IIDIIIFAnnoMap, elementClasses?: string[]): Promise<IIDIIIFAnnoMap> {
        const updatesBufferCopy = await structuredClone(this.updatesBuffer);
        const additionsBufferCopy = await structuredClone(this.additionsBuffer);
        if (!elementClasses) {
            // Object.assign(statements, updatesBufferCopy);
            // Object.assign(statements, additionsBufferCopy);
            mergeUpdatesToResources(statements, updatesBufferCopy);
            mergeUpdatesToResources(statements, additionsBufferCopy);
        } else {
            const assignIfAsked = (statement: IIIFAnno) => {
                if (elementClasses.includes(statement.jsonClassLabel as string)) {
                    const oldStatement = statements[statement['@id']];
                    if (oldStatement) {
                        Object.assign(oldStatement, statement);
                    } else {
                        // eslint-disable-next-line no-param-reassign
                        statements[statement['@id']] = statement;
                    }
                }
            };
            Object.values(updatesBufferCopy).forEach(assignIfAsked);
            Object.values(additionsBufferCopy).forEach(assignIfAsked);
        }
        this.deletesBuffer.forEach((id) => {
            // eslint-disable-next-line no-param-reassign
            delete statements[id];
        });
        return statements;
    }

    private async flushBufferWithNoNewStrategy() {
        // first update, and then delete
        const updatesGraph = statementGraphToResourceGraph(await structuredClone(this.updatesBuffer));
        await this.postSubGraph(updatesGraph);
        this.updatesBuffer = {};
        await this.deleteSubgraph(this.deletesBuffer);
        this.deletesBuffer = [];
    }

    private async flushBufferWithComputedHierarchy(segmentsGResp: objstore.rms.GraphResponse) {
        await this.flushBufferWithNoNewStrategy();

        const additionsGraph = statementGraphToResourceGraph(await structuredClone(this.additionsBuffer));
        computeHOCRHierarchyForNewResources(this.gResp, additionsGraph, segmentsGResp);
        // console.log({ additionsGraph });
        await this.postSubGraph(additionsGraph);
        this.additionsBuffer = {};
    }

    // eslint-disable-next-line consistent-return
    public async flushBuffer(segmentsGResp: objstore.rms.GraphResponse) {
        if (Object.keys(this.additionsBuffer).length === 0) {
            await this.flushBufferWithNoNewStrategy();
            this.clearBuffer();
            this.emit(LayoutStatementsStore.BUFFER_FLUSHED, 1);
        } else {
            await this.flushBufferWithComputedHierarchy(segmentsGResp);
            this.clearBuffer();
            this.emit(LayoutStatementsStore.BUFFER_FLUSHED, 2);
        }
    }
}
