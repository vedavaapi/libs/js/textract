import { IIDResourceMap } from '@vedavaapi/client/dist-esm/api/api-param-models';
import { objstore } from '@vedavaapi/client';
import { OID } from '@vedavaapi/types/dist-esm/JsonObject';
import { structuredClone, mergeUpdatesToResources } from './graph-helper';
import { GraphStore } from './graph-store';

/**
 * BufferredStores Should only layer over base GraphStore. access gResp synchronously for readonly purposes. Any muttions to gResp should go through base GraphStore methods
 * And thus it can only buffer update operations, but not create and delete ops, as they change existense of a resource, not just props of resource. hence client should perform them directly.
 * Any delete ops over already buffered resources will be synced, after they completed
 */
export class BufferredGraphStore extends GraphStore {
    private updatesBuffer: IIDResourceMap;

    private deleteListener: any;

    private refreshListener: any;

    public static readonly BUFFER_FLUSHED = 'buffer-flushed';

    public static readonly BUFFER_FORCE_CLEARED = 'buffer-force-cleared';

    public static readonly BUFFER_CLEARED = 'buffer-cleared';

    public static readonly BUFFER_UPDATE = 'buffer_update';

    constructor(vvinfo: any, pageId: OID) {
        super(vvinfo, pageId);
        this.updatesBuffer = {};
        this.deleteListener = (allDeletedIds: OID[]) => {
            allDeletedIds.forEach((id) => {
                if (Object.prototype.hasOwnProperty.call(this.updatesBuffer, id)) {
                    delete this.updatesBuffer[id];
                }
            });
        };
        this.refreshListener = () => {
            if (!this.isBufferDirty) return;
            this.updatesBuffer = {};
            this.emit(BufferredGraphStore.BUFFER_FORCE_CLEARED);
        };

        this.on(BufferredGraphStore.RESOURCES_DELETED, this.deleteListener);
        this.on(BufferredGraphStore.RESOURCES_REFRESHED, this.refreshListener);
    }

    public isBufferDirty(): boolean {
        return Object.keys(this.updatesBuffer).length > 0;
    }

    public async bufferUpdates(graph: IIDResourceMap) {
        await this.load();
        // checking all before buffering any, to be atomic on error
        if (!Object.keys(graph).every((id) => Object.prototype.hasOwnProperty.call(this.gResp.graph, id))) {
            throw Error('Only those resources which already existed in graph, can be bufferred');
        }
        const graphCopy = await structuredClone(graph);
        Object.assign(this.updatesBuffer, graphCopy);
        this.emit(BufferredGraphStore.BUFFER_UPDATE, await structuredClone(graph));
    }

    public async getBuffer(): Promise<IIDResourceMap> {
        return structuredClone(this.updatesBuffer);
    }

    public async mergeBuffer(gResp: objstore.rms.GraphResponse): Promise<objstore.rms.GraphResponse> {
        const bufferCopy = await structuredClone(this.updatesBuffer);
        // Object.assign(gResp.graph, bufferCopy);
        mergeUpdatesToResources(gResp.graph, bufferCopy);
        return gResp;
    }

    public async flushBuffer() {
        await this.load();
        await this.postSubGraph(this.updatesBuffer);
        this.updatesBuffer = {};
        this.emit(BufferredGraphStore.BUFFER_FLUSHED, await structuredClone(this.updatesBuffer));
    }

    public async clearBuffer() {
        const bufferCopy = await structuredClone(this.updatesBuffer);
        this.updatesBuffer = {};
        this.emit(BufferredGraphStore.BUFFER_CLEARED, bufferCopy);
    }
}
