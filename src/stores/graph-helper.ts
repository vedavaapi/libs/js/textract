/* eslint-disable no-underscore-dangle */
import delve from 'dlv';
import { OID } from '@vedavaapi/types/dist-esm/JsonObject';
import { objstore } from '@vedavaapi/client';
import { IResource } from '@vedavaapi/types/dist-esm/Resource';
import { IIDResourceMap } from '@vedavaapi/client/dist/api/api-param-models';


export function structuredClone<T>(obj: T): Promise<T> {
    // check here https://github.com/whatwg/html/issues/793#issuecomment-359758310
    // here too https://stackoverflow.com/a/10916838/5925119
    return new Promise((resolve) => {
        const { port1, port2 } = new MessageChannel();
        port2.onmessage = (ev) => {
            const newObj = Object.create(Object.getPrototypeOf(obj));
            Object.assign(newObj, ev.data);
            resolve(newObj);
        };
        port1.postMessage(obj);
    });
}

/**
 * following all functions takes result actual graphs to merge and sync. not blank graphs
 * they should only modify gResp, if required. should clone any other params.
 */


export async function upsertResourcesToGraphResponse(gResp: objstore.rms.GraphResponse, graph: IIDResourceMap): Promise<IIDResourceMap> {
    // this iteraton copies __reached_ids to updated resources, if they already exist in gResp.
    // we are not updating _reached_ids of parent resources of new resources in this iteration, as there may be child parent relations in new graph too. thus we first safely copy all into gResp and do this op over all.
    // eslint-disable-next-line no-param-reassign
    graph = await structuredClone(graph) as unknown as IIDResourceMap;
    const finalResGraph: IIDResourceMap = {};
    Object.keys(graph).forEach((id) => {
        const res = graph[id];
        const oldRes = gResp.graph[id];
        if (oldRes) {
            res._reached_ids = oldRes._reached_ids;
            Object.assign(oldRes, res); // NOTE: we are merging update, instead of replacing
        } else {
            // eslint-disable-next-line no-param-reassign
            gResp.graph[id] = res;
        }
        finalResGraph[id] = gResp.graph[id];
    });
    Object.keys(graph).forEach((id) => {
        const res = gResp.graph[id];
        ['source', 'target'].forEach((by) => {
            const parentId = res[by]; // we are assuming it to be one, in this case, it is true
            if (!parentId) return;
            const parent = gResp.graph[parentId];
            if (!parent) return;
            parent._reached_ids = parent._reached_ids || {};
            parent._reached_ids[by] = parent._reached_ids[by] || [];
            if (!parent._reached_ids[by].includes(id)) parent._reached_ids[by].push(id);
        });
    });
    return structuredClone(finalResGraph) as unknown as IIDResourceMap;
}


/**
 * merges negetive diff
 */
export async function deleteResourcesFromGraphResponse(gResp: objstore.rms.GraphResponse, resIds: OID[]): Promise<objstore.pms.IIDResourceMap> {
    const delResMap: IIDResourceMap = {};

    const delRes = (resId: OID) => {
        const res = gResp.graph[resId];
        if (!res) return;
        // eslint-disable-next-line no-param-reassign
        delete gResp.graph[resId];
        delResMap[resId] = res;
        ['source', 'target'].forEach((by) => {
            const parentId = res[by];
            if (!parentId) return;
            const parent = gResp.graph[parentId];
            if (!parent) return;
            const parentReachedIds = delve(parent, `_reached_ids.${by}`);
            parentReachedIds.pop(resId);

            const resReachedIds: OID[] = delve(res, `_reached_ids.${by}`, []);
            resReachedIds.forEach((cid) => delRes(cid));
        });
    };

    resIds.forEach((resId) => delRes(resId));
    return delResMap;
}


/**
 * mostly for reading diffs
 */
export async function getSubGraphResponseFromGraphResponse(gResp: objstore.rms.GraphResponse, startNodesIds: OID[], depth = -1): Promise<objstore.rms.GraphResponse> {
    const subgraph: IIDResourceMap = {};
    const subGraphResponse = new objstore.rms.GraphResponse();
    // @ts-ignore
    subGraphResponse.start_nodes_ids = [...startNodesIds];
    subGraphResponse.graph = subgraph;

    const fillSubGraph = (startNodes: IResource[], _d: number) => {
        startNodes.forEach((node) => {
            if (!node) return;
            subgraph[node._id as OID] = node;
            if (_d === 0) return;
            fillSubGraph(gResp.getReachedResources(node, 'source'), _d - 1);
            fillSubGraph(gResp.getReachedResources(node, 'target'), _d - 1);
        });
    };

    fillSubGraph(startNodesIds.map((nodeId) => gResp.graph[nodeId]), depth);
    return structuredClone(subGraphResponse) as unknown as objstore.rms.GraphResponse;
}


export async function getIdResolvedGraph(blankGraph: IIDResourceMap, graphIdUIdMap: {[x: string]: OID}): Promise<IIDResourceMap> {
    // eslint-disable-next-line no-param-reassign
    blankGraph = await structuredClone(blankGraph) as unknown as IIDResourceMap;
    const graph: IIDResourceMap = {};
    Object.keys(blankGraph).forEach((gid) => {
        const res = blankGraph[gid];
        const uid = graphIdUIdMap[gid];
        if (!uid) return;
        res._id = uid;

        ['source', 'target'].forEach((k) => {
            const parentGId = res[k];
            if (!parentGId) return;
            res[k] = graphIdUIdMap[parentGId] || parentGId;
        });
        graph[uid] = res;
    });
    return graph;
}


export function mergeUpdatesToResources(graph: { [x: string]: any }, updatesGraph: { [x: string]: any }) {
    Object.keys(updatesGraph).forEach((resId) => {
        const res = graph[resId];
        const resUpdate = updatesGraph[resId];
        if (!res) {
            if (resUpdate) {
                // eslint-disable-next-line no-param-reassign
                graph[resId] = resUpdate;
            }
            return;
        }
        Object.assign(res, resUpdate);
    });
}
