import Emitter from 'component-emitter';
import { IIDResourceMap } from '@vedavaapi/client/dist-esm/api/api-param-models';
import { objstore, Context } from '@vedavaapi/client';
import { OID } from '@vedavaapi/types/dist-esm/JsonObject';
import { structuredClone, getSubGraphResponseFromGraphResponse, deleteResourcesFromGraphResponse, getIdResolvedGraph, upsertResourcesToGraphResponse } from './graph-helper';


/**
 * These stores should just serve as proxies for graph ops over a defined graph
 * should allow clients to do every ops over graph, with native objstore semantics, but act as middleware, merging all diffs
 * These stores should guarantee clients that, any updates happened to it's internal gResp will be notified, along with data necessary for reproducing effect on symmetrical store.
 * TODO should be fully transparent to all api options
 * TODO should retrieve created, creator etc also on post, for completeness
 * TODO optimize extraneous cloning
 */


export class GraphStore extends Emitter {
    public vvinfo: {
        vc: Context;
    };

    public pageId: OID;

    protected loadPromise: Promise<objstore.rms.GraphResponse> | null = null; // Will be used to dedupe async loading

    protected gResp!: objstore.rms.GraphResponse; // No public method should expose this source of truth out. it should be out of subscriber interface

    public static readonly RESOURCES_LOADED = 'resources-loaded';

    public static readonly RESOURCES_REFRESHED = 'resources-refreshed';

    public static readonly RESOURCES_DELETED = 'resources-deleted';

    public static readonly RESOURCES_UPDATED = 'resources-updated';


    constructor(vvinfo: any, pageId: OID, gResp?: objstore.rms.GraphResponse) {
        super();
        this.vvinfo = vvinfo;
        this.pageId = pageId;
        this.loadPromise = null;
        // @ts-ignore
        this.gResp = gResp;
    }

    protected async resolveLoadPromise() {
        if (!this.loadPromise) return;
        try {
            this.gResp = await this.loadPromise;
            this.loadPromise = null;
        } catch (e) {
            this.loadPromise = null;
            throw e;
        }
    }

    public async load(refresh = false) {
        // implementations should override this
        if (!this.gResp) {
            // get it from somewhere
            // eslint-disable-next-line no-self-assign
            this.gResp = this.gResp;
            this.emit(GraphStore.RESOURCES_LOADED);
        } else if (refresh) {
            this.emit(GraphStore.RESOURCES_REFRESHED);
        }
    }

    public async gResponse(): Promise<objstore.rms.GraphResponse> {
        await this.load();
        return structuredClone(this.gResp) as unknown as objstore.rms.GraphResponse;
    }

    public async getSubGraph(startNodesIds: OID[], depth: number): Promise<objstore.rms.GraphResponse> {
        await this.load();
        return getSubGraphResponseFromGraphResponse(this.gResp, startNodesIds, depth);
    }

    public async deleteSubgraph(startNodesIds: OID[]): Promise<objstore.rms.IResourcesDeleteResponse> {
        await this.load();
        const resp = await objstore.Resources.delete({
            vc: this.vvinfo.vc,
            data: {
                resource_ids: startNodesIds,
            },
        });
        const allDeletedIds = resp.data.all_deleted_ids;
        await deleteResourcesFromGraphResponse(this.gResp, allDeletedIds);
        this.emit(GraphStore.RESOURCES_DELETED, allDeletedIds);
        return resp.data;
    }

    public async postSubGraph(graph: IIDResourceMap): Promise<objstore.rms.IGraphPostResponse> {
        await this.load();
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const resp = await objstore.Graph.post({
            vc: this.vvinfo.vc,
            data: {
                graph,
                should_return_resources: false,
            },
        });

        const idResolvedGraph = await getIdResolvedGraph(graph, resp.data.graph as { [x: string]: string; });
        const finalResGraph = await upsertResourcesToGraphResponse(this.gResp, idResolvedGraph); // idResolvedGraph will be cloned and clone will be used
        this.emit(GraphStore.RESOURCES_UPDATED, finalResGraph);
        return resp.data;
    }
}
