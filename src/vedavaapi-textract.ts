import delve from 'dlv';
import { objstore, Context } from '@vedavaapi/client';
import { IImageRegion, bbox } from '@vedavaapi/types/dist-esm/ImageRegion';
import { IScannedPage } from '@vedavaapi/types/dist-esm/ScannedPage';
import { IResource } from '@vedavaapi/types/dist-esm/Resource';
import { ITextAnnotation } from '@vedavaapi/types/dist-esm/TextAnnotation';
import { OID } from '@vedavaapi/types/dist-esm/JsonObject';

// TODO don't retrieve page at all.
// TODO project to minimum data.
export async function getSegmentsGraph(vc: Context, pageIds: string[], init?: any) {
    return (await objstore.Graph.put({
        vc,
        data: {
            start_nodes_selector: { _id: { $in: pageIds } },
            traverse_key_filter_maps_list: [
                { source: { jsonClass: 'ImageRegion' } },
                { target: { jsonClass: 'TextAnnotation' } },
            ],
            direction: 'referrer',
            max_hops: 2,
            include_incomplete_paths: true,
            json_class_projection_map: { '*': { resolvedPermissions: 0, hierarchy: 0 } }, // TODO proper projection
        },
        init,
    })).data;
}

export async function getLayoutGraph(vc: Context, pageIds: string[], init?: any) {
    return (await objstore.Graph.put({
        vc,
        data: {
            start_nodes_selector: { _id: { $in: pageIds } },
            traverse_key_filter_maps_list: [
                { source: { jsonClass: 'Resource', jsonClassLabel: { $regex: '^HOCR' } } },
            ],
            direction: 'referrer',
            max_hops: 4,
            include_incomplete_paths: true,
            json_class_projection_map: {
                '*': { resolvedPermissions: 0, hierarchy: 0 },
            },
        },
        init,
    })).data;
}

export function orderRegionsByCoords(regions: IImageRegion[]) {
    return regions.sort((r1, r2) => {
        const bbox1 = bbox(r1);
        if (bbox1 === null) return +Infinity;
        const bbox2 = bbox(r2);
        if (bbox2 === null) return -Infinity;
        if (bbox1.y < bbox2.y) return -1;
        if (bbox1.x < bbox2.x) return 1;
        return 0;
    });
}

export function getOrderedRegions(sgResponse: objstore.rms.GraphResponse, lgResponse: objstore.rms.GraphResponse, pageId?: string) {
    let rgPageId = pageId;
    if (!rgPageId) {
        const rgStartNodesIds = sgResponse.start_nodes_ids;
        if (!rgStartNodesIds.length) {
            return null;
        }
        [rgPageId] = rgStartNodesIds;
    }

    if (!Object.prototype.hasOwnProperty.call(sgResponse.graph, rgPageId)) {
        return null;
    }

    const rgPage = sgResponse.graph[rgPageId] as IScannedPage;
    const regionIds = delve(rgPage, '_reached_ids.source', []) as OID[];
    if (regionIds.length === 0) {
        return null;
    }

    if (!Object.prototype.hasOwnProperty.call(lgResponse.graph, rgPageId)) {
        return orderRegionsByCoords(sgResponse.getReachedResources(rgPage, 'source') as IImageRegion[]);
    }

    const lgPage = lgResponse.graph[rgPageId] as IScannedPage;

    const regions: IImageRegion[] = [];
    const extendRegions = (layoutElem: IResource) => {
        if (!layoutElem) {
            return;
        }
        const membersIds = delve(layoutElem, 'members.resource_ids') as string[] | undefined;
        if (membersIds !== undefined && membersIds !== null) {
            membersIds.forEach((mid) => {
                if (Object.prototype.hasOwnProperty.call(sgResponse.graph, mid)) {
                    regions.push(sgResponse.graph[mid] as IImageRegion);
                }
            });
            return;
        }
        const reachedLayoutElems = lgResponse.getReachedResources(layoutElem, 'source') as IResource[];
        reachedLayoutElems.forEach((e) => {
            extendRegions(e);
        });
    };

    extendRegions(lgPage);
    return regions;
}

/**
 * given pageId, and Context, returns ordered {region, annos} pairs
 * @param vc Context; must contains ModulesRegistry with ScannedPage, ImageRegion, TextAnnotation, Resource registered
 * @param pageId
 * @param init fetchInit options
 */
export async function getSortedRegionAnnoPairs(vc: Context, pageId: string, init?: any) {
    const sgPromise = getSegmentsGraph(vc, [pageId], init);
    const lgPromise = getLayoutGraph(vc, [pageId], init);

    const [sgResponse, lgResponse] = await Promise.all([sgPromise, lgPromise]);
    const orderedRegions = getOrderedRegions(sgResponse, lgResponse);
    return orderedRegions ? orderedRegions.map((region) => {
        const annos = sgResponse.getReachedResources(region, 'target') as ITextAnnotation[];
        return { region, annos };
    }) : null;
}
