import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import pkg from './package.json';


export default [
    // browser friendly UMD and IIFE builds
    {
        input: pkg.module,
        output: {
            name: 'VT',
            file: 'dist-umd/vedavaapi-textract.js',
            format: 'umd',
        },
        plugins: [
            resolve(),
            commonjs(),
            /* typescript({
                // eslint-disable-next-line global-require
                typescript: require('typescript'),
            }), */
            // terser(),
        ],
        external: [
            '@vedavaapi/client',
        ],
    },
];
